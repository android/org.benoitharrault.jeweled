import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:jeweled/config/application_config.dart';
import 'package:jeweled/models/activity/cell_location.dart';
import 'package:jeweled/models/activity/activity.dart';

part 'activity_state.dart';

class ActivityCubit extends HydratedCubit<ActivityState> {
  ActivityCubit()
      : super(ActivityState(
          currentActivity: Activity.createEmpty(),
        ));

  void updateState(Activity activity) {
    emit(ActivityState(
      currentActivity: activity,
    ));
  }

  void refresh() {
    final Activity activity = Activity(
      // Settings
      activitySettings: state.currentActivity.activitySettings,
      // State
      isRunning: state.currentActivity.isRunning,
      isStarted: state.currentActivity.isStarted,
      isFinished: state.currentActivity.isFinished,
      animationInProgress: state.currentActivity.animationInProgress,
      // Base data
      board: state.currentActivity.board,
      // Game data
      shuffledColors: state.currentActivity.shuffledColors,
      availableBlocksCount: state.currentActivity.availableBlocksCount,
      score: state.currentActivity.score,
      movesCount: state.currentActivity.movesCount,
    );
    // game.dump();

    updateState(activity);
  }

  void startNewActivity(BuildContext context) {
    final ActivitySettingsCubit activitySettingsCubit =
        BlocProvider.of<ActivitySettingsCubit>(context);

    final Activity newActivity = Activity.createNew(
      // Settings
      activitySettings: activitySettingsCubit.state.settings,
    );

    newActivity.dump();

    updateState(newActivity);
    postAnimate();
  }

  bool canBeResumed() {
    return state.currentActivity.canBeResumed;
  }

  void quitActivity() {
    state.currentActivity.isRunning = false;
    refresh();
  }

  void resumeSavedActivity() {
    state.currentActivity.isRunning = true;
    refresh();
  }

  void deleteSavedActivity() {
    state.currentActivity.isRunning = false;
    state.currentActivity.isFinished = true;
    refresh();
  }

  void updateCellValue(CellLocation locationToUpdate, int? value) {
    state.currentActivity.updateCellValue(locationToUpdate, value);
    refresh();
  }

  void increaseMovesCount() {
    state.currentActivity.isStarted = true;
    state.currentActivity.increaseMovesCount();
    refresh();
  }

  void increaseScore(int increment) {
    state.currentActivity.increaseScore(increment);
    refresh();
  }

  void updateAvailableBlocksCount() {
    state.currentActivity.updateAvailableBlocksCount();
    refresh();
  }

  void updateGameIsFinished(bool gameIsFinished) {
    state.currentActivity.isFinished = gameIsFinished;
    refresh();
  }

  void shuffleColors(final String colorTheme) {
    state.currentActivity.shuffleColorsAgain(colorTheme);
  }

  moveCellsDown() {
    final Activity currentActivity = state.currentActivity;

    final int boardSizeHorizontal =
        currentActivity.activitySettings.getAsInt(ApplicationConfig.parameterCodeBoardSize);
    final int boardSizeVertical =
        currentActivity.activitySettings.getAsInt(ApplicationConfig.parameterCodeBoardSize);

    for (int row = 0; row < boardSizeVertical; row++) {
      for (int col = 0; col < boardSizeHorizontal; col++) {
        // empty cell?
        if (currentActivity.getCellValue(CellLocation.go(row, col)) == null) {
          // move cells down
          for (int r = row; r > 0; r--) {
            updateCellValue(CellLocation.go(r, col),
                currentActivity.getCellValue(CellLocation.go(r - 1, col)));
          }
          // fill top empty cell
          updateCellValue(CellLocation.go(0, col),
              currentActivity.getFillValue(CellLocation.go(row, col)));
        }
      }
    }
  }

  void deleteBlock(List<CellLocation> block) {
    // Sort cells from top to bottom
    block.sort((cell1, cell2) => cell1.row.compareTo(cell2.row));
    // Delete all cells
    for (CellLocation blockItemToDelete in block) {
      updateCellValue(blockItemToDelete, null);
    }
  }

  int getScoreFromBlock(int blockSize) {
    return 3 * (blockSize - 2);
  }

  List<CellLocation> tapOnCell(CellLocation tappedCellLocation) {
    final Activity currentActivity = state.currentActivity;

    final int? cellValue = currentActivity.getCellValue(tappedCellLocation);

    if (cellValue != null) {
      List<CellLocation> blockCells = currentActivity.getSiblingCells(tappedCellLocation, []);
      if (blockCells.length >= ApplicationConfig.blockMinimumCellsCount) {
        deleteBlock(blockCells);
        increaseMovesCount();
        increaseScore(getScoreFromBlock(blockCells.length));
        return blockCells;
      }
    }

    return [];
  }

  void postAnimate() {
    moveCellsDown();
    updateAvailableBlocksCount();
    refresh();

    if (!state.currentActivity.hasAtLeastOneAvailableBlock()) {
      printlog('no more block found. finish game.');
      updateGameIsFinished(true);
    }
  }

  @override
  ActivityState? fromJson(Map<String, dynamic> json) {
    final Activity currentActivity = json['currentActivity'] as Activity;

    return ActivityState(
      currentActivity: currentActivity,
    );
  }

  @override
  Map<String, dynamic>? toJson(ActivityState state) {
    return <String, dynamic>{
      'currentActivity': state.currentActivity.toJson(),
    };
  }
}
