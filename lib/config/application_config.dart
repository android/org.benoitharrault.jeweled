import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:jeweled/cubit/activity/activity_cubit.dart';

import 'package:jeweled/ui/pages/game.dart';

import 'package:jeweled/ui/parameters/parameter_painter_board_size.dart';
import 'package:jeweled/ui/parameters/parameter_painter_colors_count.dart';
import 'package:jeweled/ui/parameters/parameter_painter_color_theme.dart';
import 'package:jeweled/ui/parameters/parameter_painter_graphic_theme.dart';

class ApplicationConfig {
  // activity parameter: colors theme values
  static const String parameterCodeColorTheme = 'global.colorTheme';
  static const String colorThemeGothicBit = 'gothic-bit';
  static const String colorThemeSweethope = 'sweethope';
  static const String colorThemeNostalgicDreams = 'nostalgic-dreams';
  static const String colorThemeArjibi8 = 'arjibi8';

  // activity parameter: graphic theme values
  static const String parameterCodeGraphicsTheme = 'global.graphicTheme';
  static const String graphicThemeSolidBackground = 'SolidBackground';
  static const String graphicThemeGradientAndBorder = 'GradientAndBorder';
  static const String graphicThemeEmojis = 'Emojis';
  static const String graphicThemePatterns = 'Patterns';

  // activity parameter: board size values
  static const String parameterCodeBoardSize = 'activity.boardSize';
  static const String boardSizeValueSmall = '6';
  static const String boardSizeValueMedium = '10';
  static const String boardSizeValueLarge = '14';
  static const String boardSizeValueExtraLarge = '18';

  // activity parameter: colors count values
  static const String parameterCodeColorsCount = 'activity.colorsCount';
  static const String colorsCountValueLow = '5';
  static const String colorsCountValueMedium = '6';
  static const String colorsCountValueHigh = '7';
  static const String colorsCountValueVeryHigh = '8';

  static int blockMinimumCellsCount = 3;

  // activity pages
  static const int activityPageIndexHome = 0;
  static const int activityPageIndexGame = 1;

  static final ApplicationConfigDefinition config = ApplicationConfigDefinition(
    appTitle: 'Jeweled',
    activitySettings: [
      // board size
      ApplicationSettingsParameter(
        code: parameterCodeBoardSize,
        values: [
          ApplicationSettingsParameterItemValue(
            value: boardSizeValueSmall,
            color: Colors.green,
          ),
          ApplicationSettingsParameterItemValue(
            value: boardSizeValueMedium,
            color: Colors.orange,
            isDefault: true,
          ),
          ApplicationSettingsParameterItemValue(
            value: boardSizeValueLarge,
            color: Colors.red,
          ),
          ApplicationSettingsParameterItemValue(
            value: boardSizeValueExtraLarge,
            color: Colors.purple,
          ),
        ],
        customPainter: (context, value) {
          return ParameterPainterBoardSize(value: value, context: context);
        },
      ),

      // colors count
      ApplicationSettingsParameter(
        code: parameterCodeColorsCount,
        values: [
          ApplicationSettingsParameterItemValue(
            value: colorsCountValueLow,
            color: Colors.green,
          ),
          ApplicationSettingsParameterItemValue(
            value: colorsCountValueMedium,
            color: Colors.orange,
            isDefault: true,
          ),
          ApplicationSettingsParameterItemValue(
            value: colorsCountValueHigh,
            color: Colors.red,
          ),
          ApplicationSettingsParameterItemValue(
            value: colorsCountValueVeryHigh,
            color: Colors.purple,
          ),
        ],
        customPainter: (context, value) {
          return ParameterPainterColorsCount(value: value, context: context);
        },
      ),

      // colors theme
      ApplicationSettingsParameter(
        code: parameterCodeColorTheme,
        displayedOnTop: false,
        values: [
          ApplicationSettingsParameterItemValue(
            value: colorThemeGothicBit,
          ),
          ApplicationSettingsParameterItemValue(
            value: colorThemeSweethope,
            isDefault: true,
          ),
          ApplicationSettingsParameterItemValue(
            value: colorThemeNostalgicDreams,
          ),
          ApplicationSettingsParameterItemValue(
            value: colorThemeArjibi8,
          ),
        ],
        customPainter: (context, value) {
          return ParameterPainterColorTheme(value: value, context: context);
        },
      ),

      // graphics theme
      ApplicationSettingsParameter(
        code: parameterCodeGraphicsTheme,
        displayedOnTop: false,
        values: [
          ApplicationSettingsParameterItemValue(
            value: graphicThemeSolidBackground,
          ),
          ApplicationSettingsParameterItemValue(
            value: graphicThemeGradientAndBorder,
            isDefault: true,
          ),
          ApplicationSettingsParameterItemValue(
            value: graphicThemeEmojis,
          ),
          ApplicationSettingsParameterItemValue(
            value: graphicThemePatterns,
          ),
        ],
        customPainter: (context, value) {
          return ParameterPainterGraphicTheme(value: value, context: context);
        },
      ),
    ],
    startNewActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).startNewActivity(context);
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexGame);
    },
    quitCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).quitActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexHome);
    },
    deleteCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).deleteSavedActivity();
    },
    resumeActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).resumeSavedActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexGame);
    },
    navigation: ApplicationNavigation(
      screenActivity: ScreenItem(
        code: 'screen_activity',
        icon: Icon(UniconsLine.home),
        screen: ({required ApplicationConfigDefinition appConfig}) =>
            ScreenActivity(appConfig: appConfig),
      ),
      screenSettings: ScreenItem(
        code: 'screen_settings',
        icon: Icon(UniconsLine.setting),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenSettings(),
      ),
      screenAbout: ScreenItem(
        code: 'screen_about',
        icon: Icon(UniconsLine.info_circle),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenAbout(),
      ),
      activityPages: {
        activityPageIndexHome: ActivityPageItem(
          code: 'page_home',
          icon: Icon(UniconsLine.home),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageParameters(
                  appConfig: appConfig,
                  canBeResumed: activityState.currentActivity.canBeResumed,
                );
              },
            );
          },
        ),
        activityPageIndexGame: ActivityPageItem(
          code: 'page_game',
          icon: Icon(UniconsLine.star),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageGame();
              },
            );
          },
        ),
      },
    ),
  );

  static const List<String> graphicThemeContentEmojiStrings = [
    '🍏',
    '🤍',
    '🦋',
    '🐞',
    '⭐',
    '🍄',
    '🍒',
    '🐤',
  ];
  static const List<String> graphicThemeContentPatternStrings = [
    '✖',
    '✚',
    '▲',
    '■',
    '●',
    '◆',
    '━',
    '⧧',
  ];
}
