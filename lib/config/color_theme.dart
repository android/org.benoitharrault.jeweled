import 'package:jeweled/config/application_config.dart';

class ColorTheme {
  static const Map<String, List<int>> colorThemes = {
    // legacy
    // 0:[0x9D9D9D,0xFFFFFF,0xBE2633,0xE06F8B,0x493C2B,0xA46422,0xEB8931,0xF7E26B,0x2F484E,0x44891A,0xA3CE27,0x1B2632,0x005784,0x31A2F2,0xB2DCEF,],

    // https://lospec.com/palette-list/gothic-bit
    ApplicationConfig.colorThemeGothicBit: [
      0x0e0e12,
      0x1a1a24,
      0x333346,
      0x535373,
      0x8080a4,
      0xa6a6bf,
      0xc1c1d2,
      0xe6e6ec,
    ],

    // https://lospec.com/palette-list/sweethope
    ApplicationConfig.colorThemeSweethope: [
      0x615e85,
      0x9c8dc2,
      0xd9a3cd,
      0xebc3a7,
      0xe0e0dc,
      0xa3d1af,
      0x90b4de,
      0x717fb0,
    ],

    // https://lospec.com/palette-list/nostalgic-dreams
    ApplicationConfig.colorThemeNostalgicDreams: [
      0xd9af80,
      0xb07972,
      0x524352,
      0x686887,
      0x7f9bb0,
      0xbfd4b0,
      0x90b870,
      0x628c70,
    ],

    // https://lospec.com/palette-list/arjibi8
    ApplicationConfig.colorThemeArjibi8: [
      0x8bc7bf,
      0x5796a1,
      0x524bb3,
      0x471b6e,
      0x702782,
      0xb0455a,
      0xde8b6f,
      0xebd694,
    ],

    // https://lospec.com/palette-list/kotomasho-8
    // 5:[0x40263e,0x5979a6,0x84c2a3,0xefe8c3,0xefefef,0xcbc7d6,0xd06060,0x773971,],

    // https://lospec.com/palette-list/desatur8
    // 6:[0xf0f0eb,0xffff8f,0x7be098,0x849ad8,0xe8b382,0xd8828e,0xa776c1,0x545155,],

    // https://lospec.com/palette-list/purplemorning8
    // 7:[0x211d38,0x2e2a4f,0x3b405e,0x60556e,0x9a6278,0xc7786f,0xcfa98a,0xcdd4a5,],

    // https://lospec.com/palette-list/cold-war-8
    // 8:[0x332422,0xc95b40,0xff9b5e,0xfcdf76,0x4c2f7f,0x3a66ad,0x39cec2,0xfafff9,],

    // https://lospec.com/palette-list/low-8
    // 9:[0x111323,0x374566,0x50785d,0x8497b3,0xe8dcd8,0xcfb463,0xb35447,0x692e4b,],
  };

  static const int defaultThemeColor = 0x808080;
}
