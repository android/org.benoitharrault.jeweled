import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';
import 'package:jeweled/config/application_config.dart';

import 'package:jeweled/cubit/activity/activity_cubit.dart';
import 'package:jeweled/models/activity/cell_location.dart';
import 'package:jeweled/models/activity/activity.dart';
import 'package:jeweled/ui/painters/game_board_painter.dart';

class BoardWidget extends StatefulWidget {
  const BoardWidget({super.key});

  @override
  State<BoardWidget> createState() => _BoardWidget();
}

class _BoardWidget extends State<BoardWidget> with TickerProviderStateMixin {
  final int animationDuration = 500;

  List<List<Animation<double>?>> animations = [];

  void resetAnimations(int boardSize) {
    animations = List.generate(
      boardSize,
      (i) => List.generate(
        boardSize,
        (i) => null,
      ),
    );
  }

  void animateCells(List<CellLocation> cellsToRemove) {
    final ActivityCubit activityCubit = BlocProvider.of<ActivityCubit>(context);
    final Activity currentActivity = activityCubit.state.currentActivity;

    // "move down" cells
    final controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: animationDuration),
    )..addListener(() {
        if (mounted) {
          setState(() {});
        }
      });

    if (mounted) {
      setState(() {});
    }

    Animation<double> animation(int count) => Tween(
          begin: 0.0,
          end: count.toDouble(),
        ).animate(CurvedAnimation(
          curve: Curves.bounceOut,
          parent: controller,
        ))
          ..addStatusListener((status) {
            if (status == AnimationStatus.completed) {
              activityCubit.postAnimate();

              resetAnimations(currentActivity.activitySettings
                  .getAsInt(ApplicationConfig.parameterCodeBoardSize));
              setState(() {});

              controller.dispose();
            }
          });

    // Count translation length for each cell to move
    final List<List<int>> stepsDownCounts = List.generate(
      currentActivity.activitySettings.getAsInt(ApplicationConfig.parameterCodeBoardSize),
      (i) => List.generate(
        currentActivity.activitySettings.getAsInt(ApplicationConfig.parameterCodeBoardSize),
        (i) => 0,
      ),
    );
    for (CellLocation cellToRemove in cellsToRemove) {
      for (int i = 0; i < cellToRemove.row; i++) {
        stepsDownCounts[(cellToRemove.row - i) - 1][cellToRemove.col] += 1;
      }
    }

    // Build animation for each cell to move
    bool needAnimation = false;
    for (CellLocation cellToRemove in cellsToRemove) {
      for (int i = 0; i < cellToRemove.row; i++) {
        final int stepsCount = stepsDownCounts[(cellToRemove.row - i) - 1][cellToRemove.col];
        animations[(cellToRemove.row - i) - 1][cellToRemove.col] = animation(stepsCount);
        needAnimation = true;
      }
    }

    controller.forward().orCancel;

    if (!needAnimation) {
      activityCubit.postAnimate();
    }
  }

  @override
  Widget build(BuildContext context) {
    final double displayWidth = MediaQuery.of(context).size.width;

    return Center(
      child: BlocBuilder<ActivityCubit, ActivityState>(
        builder: (BuildContext context, ActivityState activityState) {
          final Activity currentActivity = activityState.currentActivity;

          if (animations.isEmpty) {
            resetAnimations(currentActivity.activitySettings
                .getAsInt(ApplicationConfig.parameterCodeBoardSize));
          }

          return GestureDetector(
            onTapUp: (details) {
              bool animationInProgress = false;
              for (List<Animation<double>?> row in animations) {
                for (Animation<double>? cell in row) {
                  if (cell != null) {
                    animationInProgress = true;
                  }
                }
              }

              if (!animationInProgress) {
                final double xTap = details.localPosition.dx;
                final double yTap = details.localPosition.dy;
                final int col = xTap ~/
                    (displayWidth /
                        currentActivity.activitySettings
                            .getAsInt(ApplicationConfig.parameterCodeBoardSize));
                final int row = yTap ~/
                    (displayWidth /
                        currentActivity.activitySettings
                            .getAsInt(ApplicationConfig.parameterCodeBoardSize));

                animateCells(BlocProvider.of<ActivityCubit>(context)
                    .tapOnCell(CellLocation.go(row, col)));
              }
            },
            child: CustomPaint(
              size: Size(displayWidth, displayWidth),
              willChange: false,
              painter: GameBoardPainter(
                activity: currentActivity,
                animations: animations,
              ),
              isComplex: true,
            ),
          );
        },
      ),
    );
  }
}
