import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';
import 'package:jeweled/config/application_config.dart';

import 'package:jeweled/cubit/activity/activity_cubit.dart';
import 'package:jeweled/models/activity/activity.dart';

class ShuffleButton extends StatelessWidget {
  const ShuffleButton({super.key, required this.activity});

  final Activity activity;

  @override
  Widget build(BuildContext context) {
    const Color textColor = Color.fromARGB(255, 215, 1, 133);
    final Color outlineColor = textColor.darken();

    return TextButton(
      child: OutlinedText(
        text: '🔄',
        fontSize: 25,
        textColor: textColor,
        outlineColor: outlineColor,
      ),
      onPressed: () {
        BlocProvider.of<ActivityCubit>(context).shuffleColors(
            activity.activitySettings.get(ApplicationConfig.parameterCodeColorTheme));
      },
    );
  }
}
