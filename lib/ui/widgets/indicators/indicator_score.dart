import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:jeweled/models/activity/activity.dart';

class ScoreIndicator extends StatelessWidget {
  const ScoreIndicator({super.key, required this.activity});

  final Activity activity;

  @override
  Widget build(BuildContext context) {
    const Color baseColor = Color.fromARGB(255, 218, 218, 218);
    final Color outlineColor = baseColor.darken();

    return OutlinedText(
      text: activity.score.toString(),
      fontSize: 70,
      textColor: baseColor,
      outlineColor: outlineColor,
    );
  }
}
