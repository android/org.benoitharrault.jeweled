import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:jeweled/models/activity/activity.dart';

class MovesCountsIndicator extends StatelessWidget {
  const MovesCountsIndicator({super.key, required this.activity});

  final Activity activity;

  @override
  Widget build(BuildContext context) {
    const Color baseColor = Color.fromARGB(255, 215, 1, 133);
    final Color outlineColor = baseColor.darken();

    return OutlinedText(
      text: activity.movesCount.toString(),
      fontSize: 40,
      textColor: baseColor,
      outlineColor: outlineColor,
    );
  }
}
