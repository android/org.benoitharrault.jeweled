import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:jeweled/models/activity/activity.dart';

class AvailableBlocksCountIndicator extends StatelessWidget {
  const AvailableBlocksCountIndicator({super.key, required this.activity});

  final Activity activity;

  @override
  Widget build(BuildContext context) {
    // Minimum available blocks count value to fill bar
    const minBlocksCount = 6;

    // Normalized [0..1] value
    final double barValue =
        min(minBlocksCount, activity.availableBlocksCount) / minBlocksCount;

    // Bar color: red < orange < green
    final Color baseColor = (barValue < 0.5
            ? Color.lerp(Colors.red, Colors.orange, barValue * 2)
            : Color.lerp(Colors.orange, Colors.green, (barValue - 0.5) * 2)) ??
        Colors.grey;

    const barHeight = 25.0;
    const Color textColor = Color.fromARGB(255, 238, 238, 238);
    const Color outlineColor = Color.fromARGB(255, 200, 200, 200);

    return Stack(
      alignment: Alignment.center,
      children: [
        LinearProgressIndicator(
          value: barValue,
          color: baseColor,
          backgroundColor: baseColor.darken(),
          minHeight: barHeight,
          borderRadius: const BorderRadius.all(Radius.circular(15)),
        ),
        OutlinedText(
          text: activity.availableBlocksCount.toString(),
          fontSize: barHeight,
          textColor: textColor,
          outlineColor: outlineColor,
        ),
      ],
    );
  }
}
