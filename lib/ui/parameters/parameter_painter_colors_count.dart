import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:jeweled/config/application_config.dart';
import 'package:jeweled/utils/color_theme_utils.dart';

class ParameterPainterColorsCount extends CustomPainter {
  const ParameterPainterColorsCount({
    required this.context,
    required this.value,
  });

  final BuildContext context;
  final String value;

  @override
  void paint(Canvas canvas, Size size) {
    // force square
    final double canvasSize = min(size.width, size.height);

    final ActivitySettings activitySettings =
        BlocProvider.of<ActivitySettingsCubit>(context).state.settings;

    final paint = Paint();
    paint.strokeJoin = StrokeJoin.round;
    paint.strokeWidth = 3;

    // Colors preview
    const List<Offset> positions = [
      Offset(0, 0),
      Offset(1, 0),
      Offset(2, 0),
      Offset(2, 1),
      Offset(2, 2),
      Offset(1, 2),
      Offset(0, 2),
      Offset(0, 1),
    ];

    final double padding = 4 / 100 * canvasSize;
    final double margin = 3 / 100 * canvasSize;
    final double width = ((canvasSize - 2 * padding) / 3) - 2 * margin;

    final colorsCount = int.parse(value);

    for (int colorIndex = 0; colorIndex < colorsCount; colorIndex++) {
      final Offset position = positions[colorIndex];

      final Offset topLeft = Offset(padding + margin + position.dx * (width + 2 * margin),
          padding + margin + position.dy * (width + 2 * margin));

      final Offset bottomRight = topLeft + Offset(width, width);

      final squareColor = Color(ColorThemeUtils.getColorCode(
          colorIndex, activitySettings.get(ApplicationConfig.parameterCodeColorTheme)));
      paint.color = squareColor;
      paint.style = PaintingStyle.fill;
      canvas.drawRect(Rect.fromPoints(topLeft, bottomRight), paint);

      final borderColor = squareColor.darken(20);
      paint.color = borderColor;
      paint.style = PaintingStyle.stroke;
      canvas.drawRect(Rect.fromPoints(topLeft, bottomRight), paint);
    }

    // centered text value
    final textSpan = TextSpan(
      text: value.toString(),
      style: TextStyle(
        color: Colors.black,
        fontSize: canvasSize / 4,
        fontWeight: FontWeight.bold,
      ),
    );
    final textPainter = TextPainter(
      text: textSpan,
      textDirection: TextDirection.ltr,
      textAlign: TextAlign.center,
    );
    textPainter.layout();
    textPainter.paint(
      canvas,
      Offset(
        (canvasSize - textPainter.width) * 0.5,
        (canvasSize - textPainter.height) * 0.5,
      ),
    );
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
