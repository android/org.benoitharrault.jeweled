import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:jeweled/config/application_config.dart';
import 'package:jeweled/utils/color_theme_utils.dart';

class ParameterPainterBoardSize extends CustomPainter {
  const ParameterPainterBoardSize({
    required this.context,
    required this.value,
  });

  final BuildContext context;
  final String value;

  @override
  void paint(Canvas canvas, Size size) {
    // force square
    final double canvasSize = min(size.width, size.height);

    final ActivitySettings activitySettings =
        BlocProvider.of<ActivitySettingsCubit>(context).state.settings;

    int gridWidth = 1;

    switch (value) {
      case ApplicationConfig.boardSizeValueSmall:
        gridWidth = 2;
        break;
      case ApplicationConfig.boardSizeValueMedium:
        gridWidth = 3;
        break;
      case ApplicationConfig.boardSizeValueLarge:
        gridWidth = 4;
        break;
      case ApplicationConfig.boardSizeValueExtraLarge:
        gridWidth = 5;
        break;
      default:
        printlog('Wrong value for boardSize parameter value: $value');
    }

    final paint = Paint();
    paint.strokeJoin = StrokeJoin.round;
    paint.strokeWidth = 3 / 100 * canvasSize;

    // Mini grid
    final borderColor = Colors.grey.shade800;

    final double cellSize = canvasSize / 7;
    final double origin = (canvasSize - gridWidth * cellSize) / 2;

    final String colorTheme = activitySettings.get(ApplicationConfig.parameterCodeColorTheme);

    for (int row = 0; row < gridWidth; row++) {
      for (int col = 0; col < gridWidth; col++) {
        final Offset topLeft = Offset(origin + col * cellSize, origin + row * cellSize);
        final Offset bottomRight = topLeft + Offset(cellSize, cellSize);

        final squareColor =
            Color(ColorThemeUtils.getColorCode(col + row * gridWidth, colorTheme));

        paint.color = squareColor;
        paint.style = PaintingStyle.fill;
        canvas.drawRect(Rect.fromPoints(topLeft, bottomRight), paint);

        paint.color = borderColor;
        paint.style = PaintingStyle.stroke;
        canvas.drawRect(Rect.fromPoints(topLeft, bottomRight), paint);
      }
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
