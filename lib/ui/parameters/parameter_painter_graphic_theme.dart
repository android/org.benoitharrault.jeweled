import 'dart:math';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:jeweled/config/application_config.dart';
import 'package:jeweled/utils/color_theme_utils.dart';

class ParameterPainterGraphicTheme extends CustomPainter {
  const ParameterPainterGraphicTheme({
    required this.context,
    required this.value,
  });

  final BuildContext context;
  final String value;

  @override
  void paint(Canvas canvas, Size size) {
    // force square
    final double canvasSize = min(size.width, size.height);

    final ActivitySettings activitySettings =
        BlocProvider.of<ActivitySettingsCubit>(context).state.settings;

    final paint = Paint();
    paint.strokeJoin = StrokeJoin.round;
    paint.strokeWidth = 3;

    // cells preview
    const List<Offset> positions = [
      Offset(0, 0),
      Offset(1, 0),
      Offset(2, 0),
      Offset(2, 1),
      Offset(2, 2),
      Offset(1, 2),
      Offset(0, 2),
      Offset(0, 1),
    ];

    final double padding = 5 / 100 * canvasSize;
    final double width = (canvasSize - 2 * padding) / 3;

    bool drawBorder = false;
    bool gradientColor = false;
    List<String> contentStrings = [];

    switch (value) {
      case ApplicationConfig.graphicThemeSolidBackground:
        break;
      case ApplicationConfig.graphicThemeGradientAndBorder:
        drawBorder = true;
        gradientColor = true;
        break;
      case ApplicationConfig.graphicThemeEmojis:
        contentStrings = ApplicationConfig.graphicThemeContentEmojiStrings;
        break;
      case ApplicationConfig.graphicThemePatterns:
        contentStrings = ApplicationConfig.graphicThemeContentPatternStrings;
        break;
      default:
        printlog('Wrong value for colorsCount parameter value: $value');
    }

    for (int itemValue = 0; itemValue < positions.length; itemValue++) {
      final Offset position = positions[itemValue];

      final Offset topLeft =
          Offset(padding + position.dx * width, padding + position.dy * width);
      final Offset bottomRight = topLeft + Offset(width, width);

      final Rect itemBox = Rect.fromPoints(topLeft, bottomRight);
      final itemColor = ColorThemeUtils.getColor(
          itemValue, activitySettings.get(ApplicationConfig.parameterCodeColorTheme));

      paint.color = itemColor;
      paint.style = PaintingStyle.fill;
      canvas.drawRect(itemBox, paint);

      // gradient background
      if (gradientColor) {
        paint.shader = ui.Gradient.linear(itemBox.topLeft, itemBox.bottomCenter, [
          itemColor.lighten(10),
          itemColor.darken(10),
        ]);
        paint.style = PaintingStyle.fill;
        canvas.drawRect(itemBox, paint);
      }

      // cell border
      if (drawBorder) {
        final paintBorder = Paint();
        paintBorder.color = itemColor.darken(20);
        paintBorder.style = PaintingStyle.stroke;
        paintBorder.strokeWidth = 2;

        canvas.drawRect(itemBox, paintBorder);
      }

      // centered text value
      if (contentStrings.isNotEmpty) {
        final textSpan = TextSpan(
          text: contentStrings[itemValue],
          style: TextStyle(
            color: Colors.black,
            fontSize: width / 2,
            fontWeight: FontWeight.bold,
          ),
        );
        final textPainter = TextPainter(
          text: textSpan,
          textDirection: TextDirection.ltr,
          textAlign: TextAlign.center,
        );
        textPainter.layout();
        textPainter.paint(
          canvas,
          Offset(
            topLeft.dx + (width - textPainter.width) * 0.5,
            topLeft.dy + (width - textPainter.height) * 0.5,
          ),
        );
      }
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
