import 'dart:math';

import 'package:flutter/material.dart';

import 'package:jeweled/utils/color_theme_utils.dart';

class ParameterPainterColorTheme extends CustomPainter {
  const ParameterPainterColorTheme({
    required this.context,
    required this.value,
  });

  final BuildContext context;
  final String value;

  @override
  void paint(Canvas canvas, Size size) {
    // force square
    final double canvasSize = min(size.width, size.height);

    const int gridWidth = 4;

    final paint = Paint();
    paint.strokeJoin = StrokeJoin.round;
    paint.strokeWidth = 3 / 100 * canvasSize;

    // Mini grid
    final borderColor = Colors.grey.shade800;

    final double cellSize = canvasSize / gridWidth;
    final double origin = (canvasSize - gridWidth * cellSize) / 2;

    for (int row = 0; row < gridWidth; row++) {
      for (int col = 0; col < gridWidth; col++) {
        final Offset topLeft = Offset(origin + col * cellSize, origin + row * cellSize);
        final Offset bottomRight = topLeft + Offset(cellSize, cellSize);

        final squareColor = Color(ColorThemeUtils.getColorCode(col + row * gridWidth, value));

        paint.color = squareColor;
        paint.style = PaintingStyle.fill;
        canvas.drawRect(Rect.fromPoints(topLeft, bottomRight), paint);

        paint.color = borderColor;
        paint.style = PaintingStyle.stroke;
        canvas.drawRect(Rect.fromPoints(topLeft, bottomRight), paint);
      }
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
