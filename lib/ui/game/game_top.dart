import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:jeweled/cubit/activity/activity_cubit.dart';
import 'package:jeweled/models/activity/activity.dart';
import 'package:jeweled/ui/widgets/indicators/indicator_available_blocks.dart';
import 'package:jeweled/ui/widgets/indicators/indicator_moves_count.dart';
import 'package:jeweled/ui/widgets/indicators/indicator_score.dart';
import 'package:jeweled/ui/widgets/indicators/indicator_shuffle_button.dart';

class GameTopWidget extends StatelessWidget {
  const GameTopWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        return Column(
          children: [
            ScoreIndicator(activity: currentActivity),
            Table(
              defaultVerticalAlignment: TableCellVerticalAlignment.middle,
              children: [
                TableRow(
                  children: [
                    Center(
                      child: MovesCountsIndicator(activity: currentActivity),
                    ),
                    AvailableBlocksCountIndicator(activity: currentActivity),
                    Center(
                      child: ShuffleButton(activity: currentActivity),
                    ),
                  ],
                )
              ],
            ),
          ],
        );
      },
    );
  }
}
