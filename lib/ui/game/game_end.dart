import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:jeweled/config/application_config.dart';

import 'package:jeweled/cubit/activity/activity_cubit.dart';

class GameEndWidget extends StatelessWidget {
  const GameEndWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        const String decorationImageAssetName = 'assets/ui/placeholder.png';

        const Image decorationImage = Image(
          image: AssetImage(decorationImageAssetName),
          fit: BoxFit.fill,
        );

        final double width = MediaQuery.of(context).size.width;

        return Container(
          margin: const EdgeInsets.all(2),
          padding: const EdgeInsets.all(2),
          child: Table(
            defaultColumnWidth: FixedColumnWidth(width / 5.1),
            defaultVerticalAlignment: TableCellVerticalAlignment.bottom,
            children: [
              TableRow(
                children: [
                  const Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [decorationImage],
                  ),
                  const Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [decorationImage],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      ActivityButtonQuit(
                        onPressed: () {
                          BlocProvider.of<ActivityCubit>(context).quitActivity();
                          BlocProvider.of<NavCubitPage>(context)
                              .updateIndex(ApplicationConfig.activityPageIndexHome);
                        },
                        color: Colors.blue,
                      )
                    ],
                  ),
                  const Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [decorationImage],
                  ),
                  const Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [decorationImage],
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
