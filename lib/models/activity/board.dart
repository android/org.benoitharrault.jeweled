import 'dart:math';

import 'package:flutter_custom_toolbox/flutter_toolbox.dart';
import 'package:jeweled/config/application_config.dart';

import 'package:jeweled/models/activity/cell.dart';

class Board {
  final List<List<Cell>> cells;

  Board({
    required this.cells,
  });

  factory Board.createEmpty() {
    return Board(cells: []);
  }

  factory Board.createRandom(ActivitySettings activitySettings) {
    final int boardSizeHorizontal =
        activitySettings.getAsInt(ApplicationConfig.parameterCodeBoardSize);
    final int boardSizeVertical =
        activitySettings.getAsInt(ApplicationConfig.parameterCodeBoardSize);
    final int maxValue = activitySettings.getAsInt(ApplicationConfig.parameterCodeColorsCount);

    final rand = Random();

    List<List<Cell>> cells = [];
    for (int rowIndex = 0; rowIndex < boardSizeVertical; rowIndex++) {
      List<Cell> row = [];
      for (int colIndex = 0; colIndex < boardSizeHorizontal; colIndex++) {
        int value = 1 + rand.nextInt(maxValue);
        row.add(Cell(value));
      }
      cells.add(row);
    }

    return Board(
      cells: cells,
    );
  }

  void dump() {
    String horizontalRule = '----';
    for (int i = 0; i < cells[0].length; i++) {
      horizontalRule += '-';
    }

    printlog('Board:');
    printlog(horizontalRule);

    for (int rowIndex = 0; rowIndex < cells.length; rowIndex++) {
      String row = '| ';
      for (int colIndex = 0; colIndex < cells[rowIndex].length; colIndex++) {
        row += cells[rowIndex][colIndex].value.toString();
      }
      row += ' |';

      printlog(row);
    }

    printlog(horizontalRule);
  }

  @override
  String toString() {
    return 'Board(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      'cells': cells.toString(),
    };
  }
}
