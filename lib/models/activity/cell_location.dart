class CellLocation {
  final int col;
  final int row;

  CellLocation({
    required this.col,
    required this.row,
  });

  factory CellLocation.go(int row, int col) {
    return CellLocation(col: col, row: row);
  }

  @override
  String toString() {
    return 'CellLocation(col: $col, row: $row)';
  }
}
