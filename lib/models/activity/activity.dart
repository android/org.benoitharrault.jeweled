import 'dart:math';

import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:jeweled/config/application_config.dart';
import 'package:jeweled/models/activity/board.dart';
import 'package:jeweled/models/activity/cell.dart';
import 'package:jeweled/models/activity/cell_location.dart';
import 'package:jeweled/utils/color_theme_utils.dart';

class Activity {
  Activity({
    // Settings
    required this.activitySettings,

    // State
    this.isRunning = false,
    this.isStarted = false,
    this.isFinished = false,
    this.animationInProgress = false,

    // Base data
    required this.board,

    // Game data
    required this.shuffledColors,
    this.availableBlocksCount = 0,
    this.score = 0,
    this.movesCount = 0,
  });

  // Settings
  final ActivitySettings activitySettings;

  // State
  bool isRunning;
  bool isStarted;
  bool isFinished;
  bool animationInProgress;

  // Base data
  final Board board;

  // Game data
  List<int> shuffledColors;
  int availableBlocksCount;
  int score;
  int movesCount;

  factory Activity.createEmpty() {
    return Activity(
      // Settings
      activitySettings: ActivitySettings.createDefault(appConfig: ApplicationConfig.config),
      // Base data
      board: Board.createEmpty(),
      // Game data
      shuffledColors: shuffleColors(ApplicationConfig.config
          .getFromCode(ApplicationConfig.parameterCodeGraphicsTheme)
          .defaultValue),
    );
  }

  factory Activity.createNew({
    ActivitySettings? activitySettings,
  }) {
    final ActivitySettings newActivitySettings = activitySettings ??
        ActivitySettings.createDefault(appConfig: ApplicationConfig.config);

    return Activity(
      // Settings
      activitySettings: newActivitySettings,
      // State
      isRunning: true,
      // Base data
      board: Board.createRandom(newActivitySettings),
      // Game data
      shuffledColors:
          shuffleColors(newActivitySettings.get(ApplicationConfig.parameterCodeColorTheme)),
    );
  }

  bool get canBeResumed => isStarted && !isFinished;

  bool get gameWon => isRunning && isStarted && isFinished;

  static List<int> shuffleColors(final String colorTheme) {
    List<int> values =
        List<int>.generate(ColorThemeUtils.getColorsCount(colorTheme), (i) => i + 1);
    values.shuffle();

    return values;
  }

  void shuffleColorsAgain(final String colorTheme) {
    shuffledColors = shuffleColors(colorTheme);
  }

  Cell getCell(CellLocation cellLocation) {
    return board.cells[cellLocation.row][cellLocation.col];
  }

  int? getCellValue(CellLocation cellLocation) {
    return getCell(cellLocation).value;
  }

  int? getCellValueShuffled(CellLocation cellLocation) {
    final int? value = getCell(cellLocation).value;
    return value != null ? shuffledColors[value - 1] : null;
  }

  void updateCellValue(CellLocation locationToUpdate, int? value) {
    board.cells[locationToUpdate.row][locationToUpdate.col].value = value;
  }

  void increaseMovesCount() {
    movesCount += 1;
  }

  void increaseScore(int? count) {
    score += (count ?? 0);
  }

  void updateAvailableBlocksCount() {
    availableBlocksCount = getAvailableBlocks(this).length;
  }

  List<CellLocation> getSiblingCells(
    final CellLocation referenceCellLocation,
    List<CellLocation> siblingCells,
  ) {
    final int boardSizeHorizontal =
        activitySettings.getAsInt(ApplicationConfig.parameterCodeBoardSize);
    final int boardSizeVertical =
        activitySettings.getAsInt(ApplicationConfig.parameterCodeBoardSize);

    final int? referenceValue = getCellValue(referenceCellLocation);

    for (int deltaRow = -1; deltaRow <= 1; deltaRow++) {
      for (int deltaCol = -1; deltaCol <= 1; deltaCol++) {
        if (deltaCol == 0 || deltaRow == 0) {
          final int candidateRow = referenceCellLocation.row + deltaRow;
          final int candidateCol = referenceCellLocation.col + deltaCol;

          if ((candidateRow >= 0 && candidateRow < boardSizeVertical) &&
              (candidateCol >= 0 && candidateCol < boardSizeHorizontal)) {
            final candidateLocation = CellLocation.go(candidateRow, candidateCol);

            if (getCellValue(candidateLocation) == referenceValue) {
              bool alreadyFound = false;
              for (int index = 0; index < siblingCells.length; index++) {
                if ((siblingCells[index].row == candidateRow) &&
                    (siblingCells[index].col == candidateCol)) {
                  alreadyFound = true;
                }
              }
              if (!alreadyFound) {
                siblingCells.add(candidateLocation);
                siblingCells = getSiblingCells(candidateLocation, siblingCells);
              }
            }
          }
        }
      }
    }

    return siblingCells;
  }

  List<List<CellLocation>> getAvailableBlocks(final Activity activity) {
    final int boardSizeHorizontal =
        activity.activitySettings.getAsInt(ApplicationConfig.parameterCodeBoardSize);
    final int boardSizeVertical =
        activity.activitySettings.getAsInt(ApplicationConfig.parameterCodeBoardSize);

    final List<List<CellLocation>> blocks = [];

    for (int row = 0; row < boardSizeVertical; row++) {
      for (int col = 0; col < boardSizeHorizontal; col++) {
        final CellLocation cellLocation = CellLocation.go(row, col);
        if (activity.getCellValue(cellLocation) != null) {
          // if current cell not already in a found block
          bool alreadyFound = false;

          for (List<CellLocation> foundBlock in blocks) {
            for (CellLocation foundBlockCell in foundBlock) {
              if ((foundBlockCell.row == row) && (foundBlockCell.col == col)) {
                alreadyFound = true;
              }
            }
          }
          if (!alreadyFound) {
            final List<CellLocation> block = activity.getSiblingCells(cellLocation, []);
            if (block.length >= 3) {
              blocks.add(block);
            }
          }
        }
      }
    }

    return blocks;
  }

  bool hasAtLeastOneAvailableBlock() {
    final int boardSizeHorizontal =
        activitySettings.getAsInt(ApplicationConfig.parameterCodeBoardSize);
    final int boardSizeVertical =
        activitySettings.getAsInt(ApplicationConfig.parameterCodeBoardSize);

    for (int row = 0; row < boardSizeVertical; row++) {
      for (int col = 0; col < boardSizeHorizontal; col++) {
        final CellLocation cellLocation = CellLocation.go(row, col);
        if (getCellValue(cellLocation) != null) {
          final List<CellLocation> block = getSiblingCells(cellLocation, []);
          if (block.length >= 3) {
            // found one block => ok, not locked
            return true;
          }
        }
      }
    }

    printlog('Board is locked!');
    return false;
  }

  bool isInBoard(CellLocation cell) {
    final int boardSize = activitySettings.getAsInt(ApplicationConfig.parameterCodeBoardSize);

    if (cell.row > 0 && cell.row < boardSize && cell.col > 0 && cell.col < boardSize) {
      return true;
    }
    return false;
  }

  int getFillValue(CellLocation referenceCellLocation) {
    final int row = referenceCellLocation.row;
    final int col = referenceCellLocation.col;

    // build a list of values to pick one
    final List<int> values = [];

    // All eligible values (twice)
    final int maxValue = activitySettings.getAsInt(ApplicationConfig.parameterCodeColorsCount);
    for (int i = 1; i <= maxValue; i++) {
      values.add(i);
      values.add(i);
    }

    // Add values of current col (twice)
    for (int r = 0;
        r <= activitySettings.getAsInt(ApplicationConfig.parameterCodeBoardSize);
        r++) {
      if (isInBoard(CellLocation.go(r, col))) {
        final int? value = getCellValue(CellLocation.go(r, col));
        if (value != null) {
          values.add(value);
          values.add(value);
        }
      }
    }

    // Add values of sibling cols (twice for top rows)
    for (int deltaCol = -1; deltaCol <= 1; deltaCol++) {
      final int c = col + deltaCol;
      for (int r = 0;
          r < activitySettings.getAsInt(ApplicationConfig.parameterCodeBoardSize);
          r++) {
        if (isInBoard(CellLocation.go(r, c))) {
          final int? value = getCellValue(CellLocation.go(r, c));
          if (value != null) {
            values.add(value);
            if (row <
                activitySettings.getAsInt(ApplicationConfig.parameterCodeBoardSize) / 3) {
              values.add(value);
            }
          }
        }
      }
    }

    // Add values of sibling cells
    for (int deltaCol = -2; deltaCol <= 2; deltaCol++) {
      final int c = col + deltaCol;
      for (int deltaRow = -2; deltaRow <= 2; deltaRow++) {
        final int r = row + deltaRow;
        if (isInBoard(CellLocation.go(r, c))) {
          final int? value = getCellValue(CellLocation.go(r, c));
          if (value != null) {
            values.add(value);
          }
        }
      }
    }

    // Pick random value from "ponderated" list
    return values[Random().nextInt(values.length)];
  }

  void dump() {
    printlog('');
    printlog('## Current game dump:');
    printlog('');
    printlog('$Activity:');
    printlog('  Settings');
    activitySettings.dump();
    printlog('  State');
    printlog('    isRunning: $isRunning');
    printlog('    isStarted: $isStarted');
    printlog('    isFinished: $isFinished');
    printlog('    animationInProgress: $animationInProgress');
    printlog('  Base data');
    board.dump();
    printlog('  Game data');
    printlog('    shuffledColors: $shuffledColors');
    printlog('    availableBlocksCount: $availableBlocksCount');
    printlog('    score: $score');
    printlog('    movesCount: $movesCount');
    printlog('');
  }

  @override
  String toString() {
    return '$Activity(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      // Settings
      'activitySettings': activitySettings.toJson(),
      // State
      'isRunning': isRunning,
      'isStarted': isStarted,
      'isFinished': isFinished,
      'animationInProgress': animationInProgress,
      // Base data
      'board': board.toJson(),
      // Game data
      'shuffledColors': shuffledColors,
      'availableBlocksCount': availableBlocksCount,
      'score': score,
      'movesCount': movesCount,
    };
  }
}
