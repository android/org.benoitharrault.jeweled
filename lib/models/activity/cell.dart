class Cell {
  int? value;

  Cell(
    this.value,
  );

  @override
  String toString() {
    return 'Cell(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      'value': value,
    };
  }
}
