import 'package:flutter/material.dart';

import 'package:jeweled/config/color_theme.dart';

class ColorThemeUtils {
  static int getColorsCount(String colorTheme) {
    if (ColorTheme.colorThemes.containsKey(colorTheme) &&
        null != ColorTheme.colorThemes[colorTheme]) {
      List<int> colors = ColorTheme.colorThemes[colorTheme] ?? [];

      return colors.length;
    }

    return 0;
  }

  static int getColorCode(int? value, String colorTheme) {
    if (value != null &&
        ColorTheme.colorThemes.containsKey(colorTheme) &&
        null != ColorTheme.colorThemes[colorTheme]) {
      List<int> skinColors = ColorTheme.colorThemes[colorTheme] ?? [];
      return (skinColors[value % getColorsCount(colorTheme)]) | 0xFF000000;
    }
    return ColorTheme.defaultThemeColor | 0xFF000000;
  }

  static Color getColor(int? value, String colorTheme) {
    return Color(getColorCode(value, colorTheme));
  }

  static Color getDefaultBorderColor() {
    return Colors.grey;
  }
}
